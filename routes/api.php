<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\ProvinceController;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\PropertyController;
use App\Http\Controllers\TypeOperationController;
use App\Http\Controllers\TypePropertyController;
use App\Http\Controllers\UserController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('login', [ApiController::class, 'authenticate']);
Route::post('register', [ApiController::class, 'register']);
Route::get('refreshToken', [ApiController::class , 'refreshToken']);
Route::post('check-token', [ApiController::class , 'checkToken']);
Route::get('tipo-operacion', [TypeOperationController::class , 'index']);
Route::get('tipo-propiedad', [TypePropertyController::class , 'index']);
Route::get('lista-provincias', [ProvinceController::class , 'index']);
Route::get('lista-localidad-provincia/{provincia}', [LocationController::class , 'listByProvince']);
Route::get('lista-localidad-todas', [LocationController::class , 'index']);

Route::get('get-coordenadas/{address}', [PropertyController::class , 'getCoordinates']);

Route::get('listar-publicaciones-user/{user_id}', [PropertyController::class , 'ListadoTodas']);
Route::get('get-publicacion-id/{property_id}', [PropertyController::class , 'getPropertyByID']);
//desplegables


Route::post('/enviar-mail', 'PropertyController@enviarMail');

Route::get('/listar-publicaciones/{tipoOperacion}/{tipoPropiedad}/{ambientes}/{dormitorios}/{provincia}/{localidad}/{tiempoPublicacion}/{limitMin}/{limitMax}/{typeQuery}', 'PropertyController@ListadoTodasFrond');
Route::get('/listar-publicaciones-mapa/{tipoOperacion}/{tipoPropiedad}/{ambientes}/{dormitorios}/{provincia}/{localidad}/{tiempoPublicacion}', 'PropertyController@ListadoTodasMapa');
Route::get('add-view/{property_id}', [PropertyController::class , 'addView']);
Route::post('/test', 'TestController@index');
Route::group(['middleware' => ['jwt.verify']], function() {
    Route::post('/testj', 'PropertyController@index');
    // Route::get('logout', [ApiController::class, 'logout']);
    // Route::get('get_user', [ApiController::class, 'get_user']);
    // Route::get('products', [ProductController::class, 'index']);
    // Route::get('products/{id}', [ProductController::class, 'show']);
    Route::post('property/create', [PropertyController::class, 'store']);
    Route::post('property/update',  [PropertyController::class, 'edit']);
    Route::post('property/delete',  [PropertyController::class, 'delete']);
    // Route::delete('delete/{product}',  [ProductController::class, 'destroy']);

    Route::post('user/update-photo',  [UserController::class, 'updatePhoto']);
    Route::post('user/update-data',  [UserController::class, 'updateData']);
    Route::post('user/update-password',  [UserController::class, 'changePassword']);
    Route::get('user/mensajes/{user_id}',  [PropertyController::class, 'mensajesUsuario']);

    Route::post('mensaje/marcar-leido',  [PropertyController::class, 'mensajeMarcarLeido']);
    Route::post('mensaje/eliminar',  [PropertyController::class, 'mensajeEliminar']);
});
