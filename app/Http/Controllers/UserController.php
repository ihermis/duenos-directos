<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
class UserController extends Controller
{
    public function updatePhoto(Request $request)
    {

        $user_id = $request->input('user_id');

        $user = User::find($user_id);
        $destinationPath = 'images/user/' . $user_id;

        $foto_actual = '';
        #echo $user_id;

        if (!is_null($user->photo) || $user->photo != '' || $user->photo != 'sin-foto.jpg') {
            $foto_actual = $user->photo;
        }

        if (!file_exists($destinationPath)) {
            mkdir(public_path($destinationPath), 666, true);
        }

        if($foto_actual != ''){
            File::delete(public_path($foto_actual));
            #\Log::info(public_path($foto_actual));
        }

        $image = $request->file('photo');
        $fileName = time() . '.' . $image->getClientOriginalExtension();
        $destinationPath = $destinationPath . '/' . $fileName;

        // $image->move(public_path('img'), $fileName);

        $img = Image::make($image->getRealPath());
        $img->resize(256, 256, function ($constraint) {
            $constraint->aspectRatio();
        });

        $img->stream();
        if ($img->save(public_path($destinationPath))) {
            $user->photo = $destinationPath;
            $user->photo_google = 0;
            $user->save();
            return response()->json(['upload' => true  , 'url_img' => $destinationPath], 200);
        } else {
            return response()->json(['upload' => false], 200);
        }

        // Storage::disk('public')->put('images/1/smalls'.'/'.$fileName, $img, 'public');
        

    }

    public function updateData(Request $request)
    {
        $user_id = $request->input('user_id');
        $user = User::find($user_id);

        $user->name = $request->input('nombre');

        if($request->input('telefono') != '')
            $user->contact_phone = $request->input('telefono');
        
        if($request->input('mail_contacto') != '')
            $user->contact_mail = $request->input('mail_contacto');

        if($user->save())
            return response()->json(['upload' => true], 200);
        else
            return response()->json(['upload' => false], 200);
    }

    public function changePassword(Request $request)
    {   
        $user_id = $request->input('user_id');
        $user = User::find($user_id);

        $user->password = bcrypt($request->input('password'));

        if($user->save())
            return response()->json(['upload' => true], 200);
        else
            return response()->json(['upload' => false], 200);
    }
}
