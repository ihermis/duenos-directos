<?php

namespace App\Http\Controllers;

use App\Antiquity;
use Illuminate\Http\Request;

class AntiquityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Antiquity  $antiquity
     * @return \Illuminate\Http\Response
     */
    public function show(Antiquity $antiquity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Antiquity  $antiquity
     * @return \Illuminate\Http\Response
     */
    public function edit(Antiquity $antiquity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Antiquity  $antiquity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Antiquity $antiquity)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Antiquity  $antiquity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Antiquity $antiquity)
    {
        //
    }
}
