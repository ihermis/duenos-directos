<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Property;
use JWTAuth;
class TestController extends Controller
{
    public function generarCoord(){
        $publicaciones = Property::with(['location', 'province'])->get();

        foreach($publicaciones as $publicacion){
            $coord = $this->getCoordinates($publicacion->address . ', ' . $publicacion->location->name . ', ' . $publicacion->province->name );
            $coord = '['.$coord[0].','.$coord[1].']';
            print_r($coord);
            echo   "<br>";
            Property::where(['id' => $publicacion->id])->update(['coord' => $coord]);


        }

    }

    public function getCoordinates($address)
    {
        $address = urlencode($address);
        $googleMapUrl = "https://maps.googleapis.com/maps/api/geocode/json?address={$address}&key=" . env('API_KEY_GMPAS');
        $geocodeResponseData = file_get_contents($googleMapUrl);
        $responseData = json_decode($geocodeResponseData, true);
        if ($responseData['status'] == 'OK') {

            $latitude = isset($responseData['results'][0]['geometry']['location']['lat']) ? $responseData['results'][0]['geometry']['location']['lat'] : "";

            $longitude = isset($responseData['results'][0]['geometry']['location']['lng']) ? $responseData['results'][0]['geometry']['location']['lng'] : "";

            $formattedAddress = isset($responseData['results'][0]['formatted_address']) ? $responseData['results'][0]['formatted_address'] : "";

            if ($latitude && $longitude && $formattedAddress) {
                $geocodeData = array();

                array_push(
                    $geocodeData,
                    $latitude,
                    $longitude,
                    $formattedAddress
                );

                return $geocodeData;
            } else {
                return false;
            }

        } else {
            #echo "ERROR: {$responseData['status']}";
            return false;
        }
    }

    public function index(Request $request){
        $status = JWTAuth::parseToken()->check(); 
        return response()->json(['token_status' => $status], 200);
        var_dump($user);
       
        // $dataRaw = explode(".",$request->token);
        //  $dataRaw =  base64_decode ($dataRaw[1]);
        //  $now = time();
        //  $dataRaw = json_decode($dataRaw);
        // echo date('d-m-Y' , $dataRaw->exp)."<br>";
        // echo date('d-m-Y' , $now-60)."<br>";
        //  if($dataRaw->exp < $now-60){
        //    return "No_Need";
        //  }
     }
}
