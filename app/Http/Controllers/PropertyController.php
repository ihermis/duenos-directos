<?php

namespace App\Http\Controllers;

use App\Mail\MailConsulta;
use App\MessageProperty;
use App\Property;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Image;

class PropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Property::all();
    }

    public function getPropertyByID($property_id)
    {
        $result = Property::where(['id' => $property_id])->with(['location', 'province', 'user', 'typeOperation', 'typeProperty'])->get()->toArray();
        return $result;
    }

    public function ListadoTodas($user_id)
    {
        $result = Property::where(["user_id" => $user_id, 'removed' => 0])->with(['location', 'province'])->orderby('updated_at', 'DESC')->get()->toArray();
        return $result;
    }

    public function ListadoTodasMapa($tipoOperacion = 0, $tipoPropiedad = 0, $ambientes = 0, $dormitorios = 0, $provincia = 0, $localidad = 0, $tiempoPublicacion = 0)
    {
        $result = Property::where("active", 1)->with(['location', 'province']);

        if ($tipoOperacion) {
            $result = $result->where("type_operation_id", 1);
        }

        if ($tipoPropiedad) {
            $result = $result->where("type_property_id", $tipoPropiedad);
        }

        if ($ambientes) {
            $result = $result->where("quantity_environment", $ambientes);
        }

        if ($dormitorios) {
            $result = $result->where("quantity_environment", $dormitorios);
        }

        if ($provincia) {
            $result = $result->where("province_id", $provincia);
        }

        if ($localidad) {
            $result = $result->where("location_id", $localidad);
        }

        if ($tiempoPublicacion) {
            $dias = Carbon::now();
            if ($tiempoPublicacion > 1) {
                $dias = Carbon::now()->subDays((int) $tiempoPublicacion);
                $result = $result->whereDate('updated_at', '>', $dias);
            } else {
                $result = $result->whereDate('updated_at', '=', $dias);
            }

        }

        $result = $result->with(['typeOperation', 'typeProperty'])->get()->toArray();

        return $result;
    }

    public function ListadoTodasFrond($tipoOperacion = 0, $tipoPropiedad = 0, $ambientes = 0, $dormitorios = 0, $provincia = 0, $localidad = 0, $tiempoPublicacion = 0, $limitMin = 0, $limitMax = 20, $typeQuery = 'list')
    {
        $result = Property::where("active", 1)->with(['location', 'province']);

        if ($tipoOperacion) {
            $result = $result->where("type_operation_id", 1);
        }

        if ($tipoPropiedad) {
            $result = $result->where("type_property_id", $tipoPropiedad);
        }

        if ($ambientes) {
            $result = $result->where("quantity_environment", $ambientes);
        }

        if ($dormitorios) {
            $result = $result->where("quantity_environment", $dormitorios);
        }

        if ($provincia) {
            $result = $result->where("province_id", $provincia);
        }

        if ($localidad) {
            $result = $result->where("location_id", $localidad);
        }

        if ($tiempoPublicacion) {
            $dias = Carbon::now();
            if ($tiempoPublicacion > 1) {
                $dias = Carbon::now()->subDays((int) $tiempoPublicacion);
                $result = $result->whereDate('updated_at', '>', $dias);
            } else {
                $result = $result->whereDate('updated_at', '=', $dias);
            }

        }
        if ($typeQuery == 'list') {
            $limitMax = $limitMax - $limitMin;

            $result = $result->with(['typeOperation', 'typeProperty'])->skip($limitMin)->take($limitMax)->get()->toArray();
        } else {

            $result = $result->with(['typeOperation', 'typeProperty'])->skip(0)->take(500)->orderBy('updated_at', 'DESC')->get()->toArray();
        }

        return $result;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function store2(Request $request)
    {

        if ($files = $request->input('images')) {
            foreach ($files as $file) {

                $destinationPath = 'images/property/test/' . $file['name'];
                Image::make(file_get_contents($file['src']))->save($destinationPath);
                #$file->move($destinationPath ,$name);

            }

        }

        return response()->json([
            'success' => true,
            'message' => 'Propiedad creada!',
        ], Response::HTTP_OK);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $property = new Property();

        $property->title = $request->input('title');
        $property->description = $request->input('description');
        $property->address = $request->input('address');
        $property->price = $request->input('price');
        $property->expenses = $request->input('expenses');
        $property->consult = $request->input('consult');
        $property->quantity_environment = $request->input('quantity_environment');
        #$property->quantity_bathrooms = $request->input('quantity_bathrooms');
        $property->quantity_bedrooms = $request->input('quantity_bedrooms');
        //$property->gallery = json_encode($request->input('gallery'));
        // $property->type_environment_id = $request->input('type_environment_id');
        $property->type_operation_id = $request->input('type_operation_id');
        $property->type_property_id = $request->input('type_environment_id');
        #$property->antiguity_id = $request->input('antiguity_id');
        $property->province_id = $request->input('province_id');
        $property->location_id = $request->input('location_id');
        $property->user_id = $request->input('user_id');
        $property->active = $request->input('active');
        $coord = $this->getCoordinates($request->input('direccion'));
        $property->coord = "[" . $coord[0] . "," . $coord[1] . "]";
        if ($property->save()) {

            if ($files = $request->input('images')) {
                $images = array();
                foreach ($files as $file) {

                    $destinationPath = 'images/property/' . $property->id;

                    if (!file_exists($destinationPath)) {
                        mkdir(public_path($destinationPath), 666, true);

                    }
                    $destinationPath = $destinationPath . '/' . $file['name'];
                    Image::make(file_get_contents($file['src']))->save(public_path($destinationPath));
                    $images[] = $destinationPath;

                }

                $property->gallery = implode('|', $images);
                $property->save();

            }
            return response()->json([
                'success' => true,
                'message' => 'Propiedad creada!',
                'data' => $property,
            ], Response::HTTP_OK);
        } else {
            return response()->json(['error' => "Ocurrio un error, volvé a intentarlo."], 200);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Property  $property
     * @return \Illuminate\Http\Response
     */
    public function show(Property $property)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Property  $property
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $property = Property::findOrFail($request->id);

        $property->title = $request->input('title');
        $property->description = $request->input('description');
        $property->address = $request->input('address');
        $property->price = $request->input('price');
        $property->expenses = $request->input('expenses');
        $property->consult = $request->input('consult');
        $property->quantity_environment = $request->input('quantity_environment');
        // $property->quantity_bathrooms = $request->input('quantity_bathrooms');
        $property->quantity_bedrooms = $request->input('quantity_bedrooms');
        //$property->gallery = json_encode($request->input('gallery'));
        // $property->type_environment_id = $request->input('type_environment_id');
        $property->type_operation_id = $request->input('type_operation_id');
        //$property->antiguity_id = $request->input('antiguity_id');
        $property->province_id = $request->input('province_id');
        $property->location_id = $request->input('location_id');
        $property->user_id = $request->input('user_id');
        $property->active = $request->input('active');

        if ($property->address != $request->input('address') || $property->coord == null || $property->coord == '') {
            $coord = $this->getCoordinates($request->input('direccion'));
            $property->coord = "[" . $coord[0] . "," . $coord[1] . "]";
        }

        if ($property->save()) {
            //cargar fotos al hostint
            if ($files = $request->input('images')) {
                $images = array();
                foreach ($files as $file) {
                    $destinationPath = 'images/property/' . $property->id;

                    if (!file_exists($destinationPath)) {
                        mkdir(public_path($destinationPath), 666, true);

                    }

                    if ($file['nuevo'] == 1) {

                        $destinationPath = $destinationPath . '/' . $file['name'];
                        Image::make(file_get_contents($file['src']))->save(public_path($destinationPath));
                    } else {
                        $destinationPath = $file['src'];
                    }

                    $images[] = $destinationPath;

                }

                $property->gallery = implode('|', $images);
                $property->save();

            }
            //cargar fotos al hostint

            if ($images = $request->input('photo_delete')) {

                foreach ($images as $image) {
                    $path = public_path($image['src']);
                    File::delete($path);
                }
            }
            return response()->json([
                'success' => true,
                'message' => 'Cambios guardados!',
                'data' => $property,
            ], Response::HTTP_OK);
        } else {
            return response()->json(['error' => "Ocurrio un error, volvé a intentarlo."], 200);
        }

    }

    public function delete(Request $request)
    {
        // $property = Property::findOrFail($request->id);
        // $property->removed = 1;

        // if ($property->save()) {
        //     MessageProperty::where(['property_id' => $request->id])->update(['removed' => 1]);
        //     return response()->json([
        //         'success' => true,
        //         'message' => 'Publicación removida!',
        //     ], Response::HTTP_OK);
        // } else {
        //     return response()->json([
        //         'success' => false,
        //         'message' => 'Intenta de nuevo, ocurrio un error.',
        //     ], Response::HTTP_OK);
        // }

        if ($propiedades = $request->input('propiedades')) {
            
            foreach ($propiedades as $propiedad) {

                $property = Property::findOrFail($propiedad['id']);
                $property->removed = 1;

                if (!$property->save()) {
                    return response()->json([
                        'success' => false,
                        'message' => 'Intenta de nuevo, ocurrio un error.',
                    ], Response::HTTP_OK);
                } 
            }
        }

        return response()->json([
            'success' => true,
            'message' => 'Publicación removida!',
        ], Response::HTTP_OK);

    }

    public function getCoordinates($address)
    {
        $address = urlencode($address);
        $googleMapUrl = "https://maps.googleapis.com/maps/api/geocode/json?address={$address}&key=" . env('API_KEY_GMPAS');
        $geocodeResponseData = file_get_contents($googleMapUrl);
        $responseData = json_decode($geocodeResponseData, true);
        if ($responseData['status'] == 'OK') {

            $latitude = isset($responseData['results'][0]['geometry']['location']['lat']) ? $responseData['results'][0]['geometry']['location']['lat'] : "";

            $longitude = isset($responseData['results'][0]['geometry']['location']['lng']) ? $responseData['results'][0]['geometry']['location']['lng'] : "";

            $formattedAddress = isset($responseData['results'][0]['formatted_address']) ? $responseData['results'][0]['formatted_address'] : "";

            if ($latitude && $longitude && $formattedAddress) {
                $geocodeData = array();

                array_push(
                    $geocodeData,
                    $latitude,
                    $longitude,
                    $formattedAddress
                );

                return $geocodeData;
            } else {
                return false;
            }

        } else {
            #echo "ERROR: {$responseData['status']}";
            return false;
        }
    }

    public function enviarMail(Request $req)
    {
        $dataMail = new \stdClass();
        $dataMail->correoInteresado = $req->email;
        $dataMail->mensaje = $req->mensaje;
        $dataMail->telefonoInteresa = $req->telefono;
        $dataMail->nombreInteresado = $req->usuarioInteresado;
        $dataMail->tituloPropiedad = $req->tituloPropiedad;

        $user = User::findorfail($req->userId);

        Mail::to($user->email)->send(new MailConsulta($dataMail));
        #print_r($mail);
        if (count(Mail::failures()) == 0) {
            $message = new MessageProperty();
            $message->email_interested = $req->email;
            $message->phone_interested = $req->telefono;
            $message->name_interested = $req->usuarioInteresado;
            $message->message = $req->mensaje;
            $message->property_id = $req->publicacionId;
            $message->save();
            return array('success' => true, "message" => "Correo enviado!");
        } else {
            return array('success' => false, "message" => "Inténtalo de nuevo, ha ocurrido un error");
        }

    }

    public function addView($property_id)
    {
        $property = Property::find($property_id);
        $property->views = (int) $property->views + 1;
        $property->save();
    }

    public function mensajesUsuario($user_id)
    {
        // $properties = Property::with(['messages' , 'province' , 'location' , 'typeProperty' , 'typeOperation'])->where(['user_id' => $user_id])->get();
        // return $properties;

        $properties = MessageProperty::with(['property'])->where(['removed' => 0])->get();
        return $properties;
    }

    public function mensajeMarcarLeido(Request $req)
    {
        $message = MessageProperty::find($req->message_id);
        $message->status = 1;

        if ($message->save()) {
            return response()->json(['status' => true, 'message' => "Marcado como leído"], 201);
        } else {
            return response()->json(['status' => false, 'message' => "Ocurrió un error, por favor intentá de nuevo"], 200);
        }
    }

    public function mensajeEliminar(Request $req)
    {

        if ($mensajes = $req->input('mensajes')) {
            
            foreach ($mensajes as $mensaje) {

                $message = MessageProperty::find($mensaje['mensaje_id']);
                $message->removed = 1;

                if (!$message->save()) {
                    return response()->json(['status' => false, 'message' => "Ocurrió un error, por favor intentá de nuevo"], 200);
                } 
            }
        }

        return response()->json(['status' => true, 'message' => "Mensaje eliminado"], 201);
    }
}
