<?php

namespace App\Http\Controllers;

use App\TypeEnvironmentProperty;
use Illuminate\Http\Request;

class TypeEnvironmentPropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TypeEnvironmentProperty  $typeEnvironmentProperty
     * @return \Illuminate\Http\Response
     */
    public function show(TypeEnvironmentProperty $typeEnvironmentProperty)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TypeEnvironmentProperty  $typeEnvironmentProperty
     * @return \Illuminate\Http\Response
     */
    public function edit(TypeEnvironmentProperty $typeEnvironmentProperty)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TypeEnvironmentProperty  $typeEnvironmentProperty
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TypeEnvironmentProperty $typeEnvironmentProperty)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TypeEnvironmentProperty  $typeEnvironmentProperty
     * @return \Illuminate\Http\Response
     */
    public function destroy(TypeEnvironmentProperty $typeEnvironmentProperty)
    {
        //
    }
}
