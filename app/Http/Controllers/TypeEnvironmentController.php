<?php

namespace App\Http\Controllers;

use App\TypeEnvironment;
use Illuminate\Http\Request;

class TypeEnvironmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TypeEnvironment  $typeEnvironment
     * @return \Illuminate\Http\Response
     */
    public function show(TypeEnvironment $typeEnvironment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TypeEnvironment  $typeEnvironment
     * @return \Illuminate\Http\Response
     */
    public function edit(TypeEnvironment $typeEnvironment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TypeEnvironment  $typeEnvironment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TypeEnvironment $typeEnvironment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TypeEnvironment  $typeEnvironment
     * @return \Illuminate\Http\Response
     */
    public function destroy(TypeEnvironment $typeEnvironment)
    {
        //
    }
}
