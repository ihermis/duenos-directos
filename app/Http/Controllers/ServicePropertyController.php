<?php

namespace App\Http\Controllers;

use App\ServiceProperty;
use Illuminate\Http\Request;

class ServicePropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ServiceProperty  $serviceProperty
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceProperty $serviceProperty)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ServiceProperty  $serviceProperty
     * @return \Illuminate\Http\Response
     */
    public function edit(ServiceProperty $serviceProperty)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ServiceProperty  $serviceProperty
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ServiceProperty $serviceProperty)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ServiceProperty  $serviceProperty
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServiceProperty $serviceProperty)
    {
        //
    }
}
