<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\Exceptions\JWTException;

class ApiController extends Controller
{
    public function register(Request $request)
    {

        if ($request->is_google) {
            $user = User::where(['email' => $request->email])->get()->toArray();

            if (count($user)) {
                $res = $this->authenticate($request);

                return $res;
            }
        }
        //Validate data
        $data = $request->only('name', 'email', 'password');
        $validator = Validator::make($data, [
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
        ]);

        /*

        'password' => 'required|string|min:6|max:50'
         */
        //Send failed response if request is not valid
        if ($validator->fails()) {
            $texto = 'El correo utilizado ya existe.';
            $user = User::where(['email' => $request->email])->first();

            if ($user->is_google) {
                $texto = 'Ya tienes una cuenta creada, ingresa con tu cuenta de google.';
            }

            return response()->json(['error' => $texto], Response::HTTP_OK);
        }

        //Request is valid, create new user
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => (isset($request->password)) ? bcrypt($request->password) : null,
            'photo' => (isset($request->photo)) ? $request->photo : 'sin-foto.jpg',
            'is_google' => $request->is_google,
            'id_google' => (isset($request->id_google)) ? $request->id_google : null,

        ]);

        if ($request->is_google) {
            $res = $this->authenticate($request);
            return $res;
        } else {
            //User created, return success response
            return response()->json([
                'success' => true,
                'message' => 'Usuario creado exitosamente!',
                'data' => $user,
            ], Response::HTTP_OK);
        }

    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        //valid credential
        $validator = Validator::make($credentials, [
            'email' => 'required|email',
            'password' => 'required|string|min:6|max:50',
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        //Request is validated
        //Crean token
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'success' => false,
                    'message' => 'Las credenciales son inválidas.',
                ], 200);
            }
        } catch (JWTException $e) {
            return $credentials;
            return response()->json([
                'success' => false,
                'message' => 'Hay un problema con la conexión, vuelve a intentar.',
            ], 200);
        }

        //Token created, return with success response and jwt token
        $user = User::where(['email' => $request->email])->first();
        return response()->json([
            'success' => true,
            'token' => $token,
            'user' => $user,
        ]);
        //        'pass' => $user->password,
    }

    public function logout(Request $request)
    {
        //valid credential
        $validator = Validator::make($request->only('token'), [
            'token' => 'required',
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        //Request is validated, do logout
        try {
            JWTAuth::invalidate($request->token);

            return response()->json([
                'success' => true,
                'message' => 'User has been logged out',
            ]);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, user cannot be logged out',
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function get_user(Request $request)
    {
        $this->validate($request, [
            'token' => 'required',
        ]);

        $user = JWTAuth::authenticate($request->token);

        return response()->json(['user' => $user]);
    }
    public function refreshToken()
    {
        $token = JWTAuth::getToken();

        try {
            $token = JWTAuth::refresh($token);
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        return response()->json(compact('token'));
    }

    public function checkToken(Request $request){
        $status = JWTAuth::parseToken()->check(); 
        return response()->json(['token_status' => $status], 200);
    }
}
