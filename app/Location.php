<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Province;
class Location extends Model
{
    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function properties()
    {
        return $this->hasMany(Property::class);
    }
}
