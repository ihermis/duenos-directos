<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    public function location()
    {
        return $this->hasMany(Location::class);
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }
}
