<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    public function typeOperation()
    {
        return $this->belongsTo(TypeOperation::class);
    }

    public function typeProperty()
    {
        return $this->belongsTo(TypeProperty::class);
    }

    public function typeEnvironment()
    {
        return $this->belongsTo(TypeEnvironment::class);
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function messages()
    {
        return $this->hasMany(MessageProperty::class);
    }
}
