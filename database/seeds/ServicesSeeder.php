<?php

use Illuminate\Database\Seeder;

class ServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('services')->insert([
            ['name' => 'Luz'],
            ['name' => 'Agua Caliente'],
            ['name' => 'Gas Natural'],
            ['name' => 'Ascensor'],
            ['name' => 'Caja Fuerte'],
            ['name' => 'Calefacción'],
            ['name' => 'Encargado / Vigilancia'],
            ['name' => 'Laundry'],
            ['name' => 'Internet / Wifi'],
        ]);
    }
}
