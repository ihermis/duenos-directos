<?php

use Illuminate\Database\Seeder;

class TypeEnvironmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('type_environments')->insert([
            ['name' => 'Cocina'],
            ['name' => 'Living comedor'],
            ['name' => 'Balcon'],
            ['name' => 'Lavadero'],
            ['name' => 'Toilette'],
            ['name' => 'Altillo'],
            ['name' => 'Baulera'],
            ['name' => 'Cocina equipada'],
            ['name' => 'Comedor'],
            ['name' => 'Jardin'],
            ['name' => 'Living'],
            ['name' => 'Patio'],
            ['name' => 'Terraza'],
            ['name' => 'Sótano'],
            ['name' => 'Vestidor'],
        ]);
    }
}
