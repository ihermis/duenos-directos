<?php

use Illuminate\Database\Seeder;

class TypePropertySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('type_properties')->insert([
            ['name' => 'Casa'],
            ['name' => 'Departamento'],
            ['name' => 'Lote'],
            ['name' => 'Oficina'],
            ['name' => 'Cochera'],
            ['name' => 'Loft'],
            ['name' => 'Local'],
            ['name' => 'PH'],
            ['name' => 'Galpón'],
        ]);
    }
}
