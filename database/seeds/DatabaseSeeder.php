<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        #$this->call(UserSeeder::class);
        $this->call(ServicesSeeder::class);
        $this->call(TypeEnvironmentSeeder::class);
        $this->call(TypeOperationSeeder::class);
        $this->call(TypePropertySeeder::class);
        $this->call(AntiquitySeeder::class);
        $this->call(CountrySeeder::class);
        $this->call(ProvinceSeeder::class);
        $this->call(LocationSeeder::class);
    }
}
