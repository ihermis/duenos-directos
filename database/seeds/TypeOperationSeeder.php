<?php

use Illuminate\Database\Seeder;

class TypeOperationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('type_operations')->insert([
            ['name' => 'Compra'],
            ['name' => 'Alquiler'],
            ['name' => 'Alquiler'],
            ['name' => 'Temporario'],
        ]);
    }
}
