<?php

use Illuminate\Database\Seeder;

class AntiquitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('antiquities')->insert([
            ['name' => '5 años'],
            ['name' => '10 añor'],
            ['name' => '15 años'],
            ['name' => '20 Años'],
            ['name' => 'Más de 25 años'],
        ]);
    }
}
