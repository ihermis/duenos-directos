<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('address');
            $table->double('price')->nulleable();
            $table->double('expenses')->nulleable();
            $table->integer('consult')->default(0);
            $table->integer('quantity_environment')->nulleable();
            $table->integer('quantity_bathrooms')->nulleable();
            $table->integer('quantity_bedrooms')->nulleable();
            $table->string('gallery');
            $table->integer('type_environment_id');
            $table->integer('type_operation_id');
            $table->integer('antiguity_id')->nulleable();
            $table->integer('province_id');
            $table->integer('location_id');
            $table->integer('user_id');
            $table->integer('active')->default(1);
            $table->string('coord')->nulleable();
            $table->integer('removed')->default(0);
            $table->integer('views')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
