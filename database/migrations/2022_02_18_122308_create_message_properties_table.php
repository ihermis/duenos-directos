<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message_properties', function (Blueprint $table) {
            $table->id();
            $table->string('email_interested');
            $table->string('phone_interested');
            $table->string('name_interested');
            $table->text('message');
            $table->int('property_id');
            $table->int('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('message_properties');
    }
}
